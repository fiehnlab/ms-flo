#!/usr/bin/env python

import cchardet as chardet
import itertools
import numpy as np
import pandas as pd
import string
import traceback

from pandas.api.types import is_numeric_dtype

try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO



def get_handler(file_format, log_function=None):
    if file_format == 'mzmine':
        return MZMinePeakTableHandler(log_function=log_function)
    elif file_format == 'msdial':
        return MSDIALPeakTableHandler(log_function=log_function)
    elif file_format == 'xcms':
        return XCMSPeakTableHandler(log_function=log_function)
    else:
        raise Exception('Invalid file format: %s' % file_format)



class PeakTableHandler:
    MSFLO_COLUMNS = ['identifier', 'duplicate_flag', 'adduct_flag', 'isotope_flag', 'isotope_phr']


    def __init__(self, log_function=None):
        self.log_function = log_function

    def log(self, msg):
        if self.log_function is not None:
            self.log_function(msg)

    def debug(self, msg):
        # Add debugging logging level
        self.log(msg)

    def read_peak_table(self, peak_table, filename=None, **kwargs):
        try:
            return pd.read_csv(peak_table, **kwargs)
        except:
            pass

        try:
            self.debug('\tUnable to read with default encoding, attempting to read as ISO-8859-1...')
            return pd.read_csv(peak_table, encoding='ISO-8859-1', **kwargs)
        except:
            self.debug('\tReading of %s failed!' % (peak_table if filename is None else filename))
            self.debug(traceback.format_exc())
            return None

    def detect_encoding(self, filename):
        if filename.endswith('.xls') or filename.endswith('.xlsx'):
            return {'encoding': None, 'confidence': 1.0}
        else:
            with open(filename, 'rb') as f:
                encoding = chardet.detect(f.read())
                self.log('Encoding type %s detected for with confidence %.1f' % (encoding['encoding'], encoding['confidence']))
                return encoding


    def unicode_to_ascii(self, s):
        printable = set(string.printable)

        return ''.join(c for c in s if c in printable)


class MZMinePeakTableHandler(PeakTableHandler):
    REQUIRED_COLUMNS = {
        'mz': 'row m/z',
        'rt': 'row retention time'
    }

    ID_COLUMN = 'row ID'
    ANNOTATION_COLUMN = 'ID'

    METADATA_COLUMNS = [
        'row ID', 'row m/z', 'row retention time',
        'row comment', 'ID', 'Name',
        'row number of detected peaks', 'All identity elements', 
        'Molecular formula', 'Identification method'
    ]


    def __init__(self, log_function=None):
        PeakTableHandler.__init__(self, log_function)


    def validate_file(self, filename):
        self.log('Validating %s as MZmine peak table...' % filename)

        # Determine encoding of the input file
        encoding = self.detect_encoding(filename)

        if encoding['encoding'] is None:
            return False

        # Validate file
        with open(filename, encoding=encoding['encoding']) as f:
            header = f.readline()
            valid = all(v in header for v in self.REQUIRED_COLUMNS.values())

            self.log('Validation result: '+ ('Success!' if valid else 'Fail!'))
            return valid


    def parse_file(self, filename):
        self.log('Opening %s as MZmine peak table...' % filename)

        # Determine encoding of the input file
        encoding = self.detect_encoding(filename)

        # Open file
        df = self.read_peak_table(filename, encoding=encoding['encoding'], quotechar='"',
                skipinitialspace=True, sep=',', skip_blank_lines=True, na_filter=True)

        if df is None:
            return 'Unable to read %s' % filename


        # Verify that all required columns are present
        for col in self.REQUIRED_COLUMNS.values():
            if col not in df.columns.values:
                return '"%s" column not found!' % col
            if df.dropna(how='all')[col].isnull().any():
                return '"%s" column has empty values!' % col

        if self.ID_COLUMN in df.columns.values and df.dropna(how='all')[self.ID_COLUMN].isnull().any():
            return '"%s" column has empty values!' % self.ID_COLUMN


        # Check that peak height columns are identifiable
        peak_columns = [s for s in df.columns.values if 'peak height' in s.lower()]

        if not peak_columns:
            peak_columns = [col for col in df.columns if col not in self.METADATA_COLUMNS + self.MSFLO_COLUMNS]

        if len(peak_columns) == 0:
            self.log('Unable to identify "Peak height" columns')
            return


        # Add a row id column if it does not exist
        if self.ID_COLUMN not in df.columns.values:
            self.log('\tCreating row id column...')
            df[self.ID_COLUMN] = 1 + df.index
        
        if self.ANNOTATION_COLUMN not in df.columns.values:
            self.log('\tCreating annotation column...')
            df[self.ANNOTATION_COLUMN] = ''


        self.log('Read %d rows' % len(df))
        self.log('Performing intitial formatting...')
        self.log('\tIdentified %d peak height columns:' % len(peak_columns))

        for col in peak_columns:
            self.log('\t\t%s' % col)

            if not is_numeric_dtype(df[col]):
                self.log('\t\t\tWARNING: Skipping non-numeric peak height column!')

        peak_columns = [col for col in peak_columns if is_numeric_dtype(df[col])]


        # Drop empty rows
        # self.debug('\tDropping empty rows')
        # self.df.dropna(subset = [self.ID_COLUMN, self.REQUIRED_COLUMNS['mz'], self.REQUIRED_COLUMNS['rt']], how = 'all', inplace = True)


        # Create temporary numeric columns for analysis
        self.debug('\tCreating processing columns')

        df['id'] = df[self.ID_COLUMN]
        df['mz'] = df[self.REQUIRED_COLUMNS['mz']]
        df['rt'] = df[self.REQUIRED_COLUMNS['rt']]
        df['id_annotation'] = df[self.ANNOTATION_COLUMN]


        # Convert m/z and retention time columns to string format
        self.debug('\tFormatting data precision')

        if 'identifier' not in df.columns.values:
            df['identifier'] = df.rt.apply(lambda n: '%0.2f' % n) +'_'+ df.mz.apply(lambda n: '%0.2f' % n)


        for col in df.columns.values:
            # Convert peak heights to integer values
            if col in peak_columns:
                df[col] = df[col].fillna(0.0).apply(lambda n: round(float(n), 2))

            # Format m/z to 4 decimals of precision
            elif col == self.REQUIRED_COLUMNS['mz']:
                if df.mz.dtype == np.float or df.mz.dtype == np.int:
                    df[col] = df.mz.apply(lambda n: '%0.4f' % n)
                else:
                    df[col] = df.mz.copy()

            # Format retention time to 3 decimals of precision
            elif col == self.REQUIRED_COLUMNS['rt']:
                if df.rt.dtype == np.float or df.rt.dtype == np.int:
                    df[col] = df.rt.apply(lambda n: '%0.3f' % n)
                else:
                    df[col] = df.rt.copy()

            # Convert all other columns to string format
            elif col != 'id' and col != 'mz' and col != 'rt' and col != 'identifier':
                df[col] = df[col].fillna('').astype(str)


        # Add column for flags if it does not exist
        self.debug('\tCreating flag columns if needed')

        if 'duplicate_flag' not in df.columns.values:
            df['duplicate_flag'] = ''

        if 'adduct_flag' not in df.columns.values:
            df['adduct_flag'] = ''

        if 'isotope_flag' not in df.columns.values:
            df['isotope_flag'] = ''
            df['isotope_phr'] = ''


        # Reorder columns
        self.debug('\tReordering columns')

        start_cols = ['identifier']
        if 'ID' in df.columns.values:
            start_cols.append('ID')
        
        start_cols += [self.ID_COLUMN, self.REQUIRED_COLUMNS['mz'], self.REQUIRED_COLUMNS['rt']]
        start_cols += ['duplicate_flag', 'adduct_flag'] + ['isotope_flag', 'isotope_phr']

        cols = [col for col in df.columns.values if col not in start_cols and col not in peak_columns and 'Unnamed' not in col]
        df = df[start_cols + cols + peak_columns]

        return (df, peak_columns)


    def export_file(self, df, filename, suffix=None):
        self.log('Performing formatting on the data frame before exporting...')

        data = df.copy()

        # Sort data by ID, placing empty strings at the end
        if 'ID' in df.columns.values:
            self.debug('\tSorting data by ID')

            if 'rt' in df.columns.values:
                data.sort_values(by=['ID', 'rt'], ascending=[False, True], inplace=True)
            else:
                data.sort_values(by=['ID', self.REQUIRED_COLUMNS['rt']], ascending=[False, True], inplace=True)
        else:
            self.debug('\tSorting data by retention time')

            if 'rt' in df.columns.values:
                data.sort_values(by='rt', inplace=True)
            else:
                data.sort_values(by=self.REQUIRED_COLUMNS['rt'], inplace=True)

        # Update annotation column
        data[self.ANNOTATION_COLUMN] = data.id_annotation

        # Drop temporary columns
        if 'mz' in df.columns.values:
            self.debug('\tDropping temporary columns')
            data.drop(['id', 'mz', 'rt', 'id_annotation'], 1, inplace=True)

        # Format filename
        f = filename.split('.')

        if suffix is not None:
            f[-2] += '_'+ suffix

        # Output file
        data.to_csv('.'.join(f), index=False)
        self.log('Wrote to %s' % ('.'.join(f)))



class MSDIALPeakTableHandler(PeakTableHandler):

    REQUIRED_COLUMNS = {
        'mz': 'Average Mz',
        'rt': 'Average Rt(min)'
    }

    METADATA_COLUMNS = [
        # General columns
        'Alignment ID', 'Average Rt(min)', 'Metabolite name', 'Fill %', 
        'INCHIKEY', 'SMILES', 'LINK', 'Dot product', 'Reverse dot product',
        'Fragment presence %', 'Spectrum reference file name', 

        # LC/MS export columns
        'Average Mz', 'Adduct ion name', 'MS/MS included', 'Isotope parent ID',
        'Isotope weight number', 'MS/MS spectrum',

        # GC/MS export columns
        'Average RI', 'Quant mass', 'Total similarity', 'RT similarity',
        'RI similarity', 'EI similarity', 'EI spectrum',

        # Legacy columns
        'SCORE'
    ]

    ID_COLUMN = 'Alignment ID'
    ANNOTATION_COLUMN = 'Metabolite name'


    def __init__(self, log_function=None):
        PeakTableHandler.__init__(self, log_function)


    def validate_file(self, filename):
        self.log('Validating %s as MS-DIAL peak table...' % filename)

        # Determine encoding of the input file
        encoding = self.detect_encoding(filename)

        if encoding['encoding'] is None:
            return False

        # Validate file
        with open(filename, encoding=encoding['encoding']) as f:
            data = self.unicode_to_ascii(f.read()).splitlines()
            valid = True

            # Determine separator
            separator = '\t'

            if separator not in data[0]:
                if ',' not in data[0]:
                    valid = False
                separator = ','

            if valid:
                while data[0].startswith(separator):
                    data.pop(0)

                valid = all(v in data[0] for v in self.REQUIRED_COLUMNS.values())

            self.log('Validation result: '+ ('Success!' if valid else 'Fail!'))
            return valid


    def parse_file(self, filename):
        self.log('Opening %s as MS-DIAL peak table...' % filename)

        # Determine encoding of the input file
        encoding = self.detect_encoding(filename)

        # Open file and string non-ascii characters
        with open(filename, encoding=encoding['encoding']) as f:
            data = self.unicode_to_ascii(f.read()).splitlines()


        # Determine separator
        separator = '\t'

        if separator not in data[0]:
            if ',' not in data[0]:
                return 'No valid separator found'
            separator = ','

        # Strip trailing separators
        trailing_separator_count = min(len(list(itertools.takewhile(lambda c: c == separator, reversed(line)))) for line in data)

        if trailing_separator_count > 0:
            self.log('Stripping %d empty columns from the end of the data file')

            data = [x[: -trailing_separator_count] for x in data]


        # Skip header table and parse data table
        peak_column_start = -1

        while data[0].startswith(separator):
            line = data.pop(0)
            peak_column_start = len(line) - len(line.lstrip(separator)) + 1


        input_stream = StringIO('\n'.join(data))
        df = self.read_peak_table(input_stream, filename=filename, quotechar='"', 
                sep=separator, skipinitialspace=True, skip_blank_lines=True, na_filter=True)

        if df is None:
            return 'Unable to read %s' % filename


        # Verify that all required columns are present
        for col in self.REQUIRED_COLUMNS.values():
            if col not in df.columns.values:
                return '"%s" column not found!' % col
            if df.dropna(how='all')[col].isnull().any():
                return '"%s" column has empty values!' % col

        if self.ID_COLUMN in df.columns.values and df.dropna(how='all')[self.ID_COLUMN].isnull().any():
            return '"%s" column has empty values!' % self.ID_COLUMN


        # Check that peak height columns are identifiable
        peak_columns = []

        if peak_column_start > -1:
            peak_columns = list(df.columns[peak_column_start:])
        else:
            peak_columns = [col for col in df.columns if col not in self.METADATA_COLUMNS + self.MSFLO_COLUMNS]

        if len(peak_columns) == 0:
            self.log('Unable to identify "Peak height" columns')
            return


        # Add a row id column if it does not exist
        if self.ID_COLUMN not in df.columns.values:
            self.log('\tCreating row id column...')
            df[self.ID_COLUMN] = 1 + df.index
        
        if self.ANNOTATION_COLUMN not in df.columns.values:
            self.log('\tCreating annotation column...')
            df[self.ANNOTATION_COLUMN] = ''


        self.log('Read %d rows' % len(df))
        self.log('Performing intitial formatting...')
        self.log('\tIdentified %d peak height columns:' % len(peak_columns))

        for col in peak_columns:
            self.log('\t\t%s' % col)

            if not is_numeric_dtype(df[col]):
                self.log('\t\t\tWARNING: Skipping non-numeric peak height column!')

        peak_columns = [col for col in peak_columns if is_numeric_dtype(df[col])]


        # Drop empty rows
        # self.debug('\tDropping empty rows')
        # self.df.dropna(subset = [self.ID_COLUMN, self.REQUIRED_COLUMNS['mz'], self.REQUIRED_COLUMNS['rt']], how = 'all', inplace = True)
        

        # Create temporary numeric columns for analysis
        self.debug('\tCreating processing columns')

        df['id'] = df[self.ID_COLUMN]
        df['mz'] = df[self.REQUIRED_COLUMNS['mz']]
        df['rt'] = df[self.REQUIRED_COLUMNS['rt']]
        df['id_annotation'] = df[self.ANNOTATION_COLUMN]

        
        # Convert m/z and retention time columns to string format
        self.debug('\tFormatting data precision')

        if 'identifier' not in df.columns.values:
            df['identifier'] = df.rt.apply(lambda n: '%0.2f' % n) +'_'+ df.mz.apply(lambda n: '%0.2f' % n)


        for col in df.columns.values:
            # Convert peak heights to integer values
            if col in peak_columns:
                df[col] = df[col].fillna(0.0).apply(lambda n: round(float(n), 2))

            # Format m/z to 4 decimals of precision
            elif col == self.REQUIRED_COLUMNS['mz']:
                if df.mz.dtype == np.float or df.mz.dtype == np.int:
                    df[col] = df.mz.apply(lambda n: '%0.4f' % n)
                else:
                    df[col] = df.mz.copy()

            # Format retention time to 3 decimals of precision
            elif col == self.REQUIRED_COLUMNS['rt']:
                if df.rt.dtype == np.float or df.rt.dtype == np.int:
                    df[col] = df.rt.apply(lambda n: '%0.3f' % n)
                else:
                    df[col] = df.rt.copy()

            # Convert all other columns to string format
            elif col != 'id' and col != 'mz' and col != 'rt' and col != 'identifier':
                df[col] = df[col].fillna('').astype(str)


        # Add column for flags if it does not exist
        self.debug('\tCreating flag columns if needed')

        if 'duplicate_flag' not in df.columns.values:
            df['duplicate_flag'] = ''

        if 'adduct_flag' not in df.columns.values:
            df['adduct_flag'] = ''

        if 'isotope_flag' not in df.columns.values:
            df['isotope_flag'] = ''
            df['isotope_phr'] = ''


        # Reorder columns
        self.debug('\tReordering columns')

        start_cols = ['identifier']
        if 'ID' in df.columns.values:
            start_cols.append('ID')
        
        start_cols += [self.ID_COLUMN, self.REQUIRED_COLUMNS['mz'], self.REQUIRED_COLUMNS['rt']]
        start_cols += ['duplicate_flag', 'adduct_flag'] + ['isotope_flag', 'isotope_phr']

        cols = [col for col in df.columns.values if col not in start_cols and col not in peak_columns and 'Unnamed' not in col]
        df = df[start_cols + cols + peak_columns]

        return (df, peak_columns)



    def export_file(self, df, filename, suffix=None):
        self.log('Performing formatting on the data frame before exporting...')

        data = df.copy()

        # Sort data by ID, placing empty strings at the end
        self.debug('\tSorting data by retention time')

        if 'rt' in df.columns.values:
            data.sort_values(by='rt', inplace=True)
        else:
            data.sort_values(by=REQUIRED_COLUMNS['rt'], inplace=True)

        # Update annotation column
        data[self.ANNOTATION_COLUMN] = data.id_annotation

        # Drop temporary columns
        if 'mz' in df.columns.values:
            self.debug('\tDropping temporary columns')
            data.drop(['id', 'mz', 'rt', 'id_annotation'], 1, inplace=True)

        # Format filename
        f = filename.split('.')

        if suffix is not None:
            f[-2] += '_'+ suffix

        # Determine separator
        separator = ',' if filename.endswith('csv') else '\t'

        # Output file
        data.to_csv('.'.join(f), sep=separator, index=False)
        self.log('Wrote to %s' % ('.'.join(f)))
        


class XCMSPeakTableHandler(PeakTableHandler):

    REQUIRED_COLUMNS = {
        'mz': 'mzmed',
        'rt': 'rtmed',
        'maxint': 'maxint'
    }

    METADATA_COLUMNS = [
        'mzmed', 'mzmin', 'mzmax', 'rtmed', 'rtmin', 'rtmax',
        'featureidx', 'fold', 'log2fold', 'tstat', 'pvalue', 'qvalue', 'updown',
        'maxint', 'isotopes', 'adduct', 'adducts', 'peakgroup', 'metlin', 'name',
        'METLIN_MSMS', 'usernotes', 'fscore', 'npeaks', 'pcgroup', 
        'Sample Classes', 'anova', 
    ]

    METADATA_COLUMNS_CONTAINS = [
        'mean', 'sd', 'Dataset'
    ]

    ID_COLUMN = ''
    ANNOTATION_COLUMN = 'name'


    def __init__(self, log_function=None):
        PeakTableHandler.__init__(self, log_function)


    def validate_file(self, filename):
        self.log('Validating %s as XCMS peak table...' % filename)

        if filename.endswith('.xlsx'):
            df = pd.read_excel(filename)
            valid = all(v in df.columns for v in self.REQUIRED_COLUMNS.values())

            self.log('Validation result: '+ ('Success!' if valid else 'Fail!'))
            return valid

        elif filename.endswith('.tsv'):
            # Determine encoding of the input file
            encoding = self.detect_encoding(filename)

            # Validate file
            with open(filename, encoding=encoding['encoding']) as f:
                header = f.readline()
                valid = all(v in header for v in self.REQUIRED_COLUMNS.values())

                self.log('Validation result: '+ ('Success!' if valid else 'Fail!'))
                return valid


    def parse_file(self, filename):
        self.log('Opening %s as XCMS peak table...' % filename)

        # Handle xlsx
        if filename.endswith('.xlsx'):
            df = pd.read_excel(filename)

        elif filename.endswith('.tsv'):
            # Determine encoding of the input file
            encoding = self.detect_encoding(filename)

            # Open file
            df = self.read_peak_table(filename, encoding=encoding['encoding'], quotechar='"',
                    skipinitialspace=True, sep='\t', skip_blank_lines=True, na_filter=True)

        else:
            self.log('Invalid file format - expected xlsx or tsv')
            df = None


        if df is None:
            return 'Unable to read %s' % filename


        # Verify that all required columns are present
        for col in self.REQUIRED_COLUMNS.values():
            if col not in df.columns.values:
                return '"%s" column not found!' % col
            if df.dropna(how='all')[col].isnull().any():
                return '"%s" column has empty values!' % col

        # Check that peak height columns are identifiable
        columns = list(df.columns)[list(df.columns).index('maxint') + 1 :]

        peak_columns = [col for col in columns if col not in self.METADATA_COLUMNS + self.MSFLO_COLUMNS]
        peak_columns = [col for col in peak_columns if all(x not in col for x in self.METADATA_COLUMNS_CONTAINS)]

        if not peak_columns:
            self.log('Unable to identify peak columns corresponding to samples, looking for class mean columns...')
            peak_columns = [col for col in columns if col not in self.METADATA_COLUMNS and 'mean' in col]


        if not peak_columns:
            self.log('Unable to identify "Peak height" columns')
            return


        # Add a row id column if it does not exist
        if self.ID_COLUMN not in df.columns.values:
            self.log('\tCreating row id column...')
            df[self.ID_COLUMN] = 1 + df.index
        
        if self.ANNOTATION_COLUMN not in df.columns.values:
            self.log('\tCreating annotation column...')
            df[self.ANNOTATION_COLUMN] = ''


        self.log('Read %d rows' % len(df))
        self.log('Performing intitial formatting...')
        self.log('\tIdentified %d peak height columns:' % len(peak_columns))

        for col in peak_columns:
            self.log('\t\t%s' % col)

            if not is_numeric_dtype(df[col]):
                self.log('\t\t\tWARNING: Skipping non-numeric peak height column!')

        peak_columns = [col for col in peak_columns if is_numeric_dtype(df[col])]

        # Drop empty rows
        # self.debug('\tDropping empty rows')
        # self.df.dropna(subset = [self.ID_COLUMN, self.REQUIRED_COLUMNS['mz'], self.REQUIRED_COLUMNS['rt']], how = 'all', inplace = True)


        # Create temporary numeric columns for analysis
        self.debug('\tCreating processing columns')

        df['id'] = df[self.ID_COLUMN]
        df['mz'] = df[self.REQUIRED_COLUMNS['mz']]
        df['rt'] = df[self.REQUIRED_COLUMNS['rt']]
        df['id_annotation'] = df[self.ANNOTATION_COLUMN]


        # Convert m/z and retention time columns to string format
        self.debug('\tFormatting data precision')

        if 'identifier' not in df.columns.values:
            df['identifier'] = df.rt.apply(lambda n: '%0.2f' % n) +'_'+ df.mz.apply(lambda n: '%0.2f' % n)


        for col in df.columns.values:
            # Convert peak heights to integer values
            if col in peak_columns:
                df[col] = df[col].fillna(0.0).apply(lambda n: round(float(n), 2))

            # Format m/z to 4 decimals of precision
            elif col == self.REQUIRED_COLUMNS['mz']:
                if df.mz.dtype == np.float or df.mz.dtype == np.int:
                    df[col] = df.mz.apply(lambda n: '%0.4f' % n)
                else:
                    df[col] = df.mz.copy()

            # Format retention time to 3 decimals of precision
            elif col == self.REQUIRED_COLUMNS['rt']:
                if df.rt.dtype == np.float or df.rt.dtype == np.int:
                    df[col] = df.rt.apply(lambda n: '%0.3f' % n)
                else:
                    df[col] = df.rt.copy()

            # Convert all other columns to string format
            elif col != 'id' and col != 'mz' and col != 'rt' and col != 'identifier':
                df[col] = df[col].fillna('').astype(str)


        # Add column for flags if it does not exist
        self.debug('\tCreating flag columns if needed')

        if 'duplicate_flag' not in df.columns.values:
            df['duplicate_flag'] = ''

        if 'adduct_flag' not in df.columns.values:
            df['adduct_flag'] = ''

        if 'isotope_flag' not in df.columns.values:
            df['isotope_flag'] = ''
            df['isotope_phr'] = ''


        # Reorder columns
        self.debug('\tReordering columns')

        start_cols = ['identifier']
        if 'ID' in df.columns.values:
            start_cols.append('ID')
        
        start_cols += [self.ID_COLUMN, self.REQUIRED_COLUMNS['mz'], self.REQUIRED_COLUMNS['rt']]
        start_cols += ['duplicate_flag', 'adduct_flag'] + ['isotope_flag', 'isotope_phr']

        cols = [col for col in df.columns.values if col not in start_cols and col not in peak_columns and 'Unnamed' not in col]
        df = df[start_cols + cols + peak_columns]

        return (df, peak_columns)


    def export_file(self, df, filename, suffix=None):
        self.log('Performing formatting on the data frame before exporting...')

        data = df.copy()

        # Sort data by ID, placing empty strings at the end
        self.debug('\tSorting data by retention time')

        if 'rt' in df.columns.values:
            data.sort_values(by='rt', inplace=True)
        else:
            data.sort_values(by=REQUIRED_COLUMNS['rt'], inplace=True)

        # Update annotation column
        data[self.ANNOTATION_COLUMN] = data.id_annotation

        # Drop temporary columns
        if 'mz' in df.columns.values:
            self.debug('\tDropping temporary columns')
            data.drop(['id', 'mz', 'rt', 'id_annotation'], 1, inplace=True)

        # Format filename
        f = filename.split('.')

        if suffix is not None:
            f[-2] += '_'+ suffix

        # Output file
        if f[-1] == 'xlsx':
            writer = pd.ExcelWriter('.'.join(f))
            data.to_excel(writer, index=False)
            writer.save()
        else:
            data.to_csv('.'.join(f), sep='\t', index=False)

        self.log('Wrote to %s' % ('.'.join(f)))


if __name__ == '__main__':
    MSDIALPeakTableHandler(print).parse_file('test/data/msdial/MX288106-Rueda-posCSH-Report_ToBeProcessed.txt')