import os
import unittest

from msflo import msflo_processor, parsers


LOG_FUNCTION = None


#
# 20160518_flagging_issue
#
class TestFlaggingIssue(unittest.TestCase):
    def test_isotope_flag_overwriting_issue(self):
        configuration_file = 'test/20160518_flagging_issue/isotope_flagging_issue.ini'
        peak_table_file = 'test/20160518_flagging_issue/isotope_flagging_issue.txt'
        output_file = 'test/20160518_flagging_issue/isotope_flagging_issue_processed.txt'

        msflo_processor.run_msflo(configuration_file, peak_table_file, 'msdial', quiet = LOG_FUNCTION is None)
        parser = parsers.get_handler('msdial', LOG_FUNCTION)

        df, _ = parser.parse_file(output_file)
        self.assertTrue(all(df['isotope_flag']))

        os.remove(output_file)
        os.remove(output_file.replace('.txt', '.log'))


#
# 20170113_annotation_issue
#
class TestAnnotationsIssue(unittest.TestCase):
    def test_annotation_deletion_issue(self):
        configuration_file = 'test/20170113_annotation_issue/annotation_issue.ini'
        peak_table_file = 'test/20170113_annotation_issue/annotation_issue.txt'
        output_file = 'test/20170113_annotation_issue/annotation_issue_processed.txt'

        msflo_processor.run_msflo(configuration_file, peak_table_file, 'msdial', quiet = LOG_FUNCTION is None)
        parser = parsers.get_handler('msdial', LOG_FUNCTION)

        df, _ = parser.parse_file(output_file)
        self.assertEqual(len(df), 1)
        self.assertEqual(df['Metabolite name'][0], 'w/o MS2:L-Methionine_L-methionineAglL')

        os.remove(output_file)
        os.remove(output_file.replace('.txt', '.log'))


#
# 20171115_column_issue
#
class TestColumnsIssue(unittest.TestCase):
    def test_msdial_column_handling_issue_corrected(self):
        configuration_file = 'test/20171115_column_issue/column_issue.ini'
        peak_table_file = 'test/20171115_column_issue/column_issue_fixed.txt'
        output_file = peak_table_file.replace('.txt', '_processed.txt')

        msflo_processor.run_msflo(configuration_file, peak_table_file, 'msdial', quiet = LOG_FUNCTION is None)
        parser = parsers.get_handler('msdial', LOG_FUNCTION)

        df, _ = parser.parse_file(output_file)
        self.assertEqual(len(df), 2)

        os.remove(output_file)
        os.remove(output_file.replace('.txt', '.log'))


    def test_msdial_column_handling_issue(self):
        configuration_file = 'test/20171115_column_issue/column_issue.ini'
        peak_table_file = 'test/20171115_column_issue/column_issue.txt'
        output_file = peak_table_file.replace('.txt', '_processed.txt')

        msflo_processor.run_msflo(configuration_file, peak_table_file, 'msdial', quiet = LOG_FUNCTION is None)
        parser = parsers.get_handler('msdial', LOG_FUNCTION)

        df, _ = parser.parse_file(output_file)
        self.assertEqual(len(df), 2)

        os.remove(output_file)
        os.remove(output_file.replace('.txt', '.log'))


#
# 20180830_validation
#
class TestValidation(unittest.TestCase):
    def test_required_column_validation(self):
        peak_table_file = 'test/20180830_validation/required_column_validation_issue.txt'

        parser = parsers.get_handler('msdial', LOG_FUNCTION)
        self.assertEqual(type(parser.parse_file(peak_table_file)), str)


#
# 20181108_processing
#

class TestProcessing(unittest.TestCase):
    def test_processing_output(self):
        configuration_file = 'test/20181108_processing/processing_params.ini'
        peak_table_file = 'test/20181108_processing/Raw MS Dial export.txt'
        output_file = 'test/20181108_processing/Raw MS Dial export_processed.txt'
        output_hash = '40dd925dab05fec5ac0c39dfc42943976d66fbe8616a8adb37c7c06fe3e61d3b'

        parser = parsers.get_handler('msdial', LOG_FUNCTION)
        self.assertTrue(parser.validate_file(peak_table_file))

        msflo_processor.run_msflo(configuration_file, peak_table_file, 'msdial', quiet = LOG_FUNCTION is None)

        df, _ = parser.parse_file(output_file)
        self.assertEqual(len(df), 947)

        os.remove(output_file)
        os.remove(output_file.replace('.txt', '.log'))

#
# 20200527_join_issue
#
class TestJoinIssue(unittest.TestCase):
    def test_required_column_validation(self):
        configuration_file = 'test/20200527_join_issue/processing_params.ini'
        peak_table_file = 'test/20200527_join_issue/adduct_join_missing_rt.txt'
        output_file = 'test/20200527_join_issue/adduct_join_missing_rt_processed.txt'

        parser = parsers.get_handler('msdial', LOG_FUNCTION)
        self.assertTrue(parser.validate_file(peak_table_file))

        msflo_processor.run_msflo(configuration_file, peak_table_file, 'msdial', quiet = False)

        df, _ = parser.parse_file(output_file)
        self.assertEqual(len(df), 2)
        self.assertTrue(df['Average Rt(min)'].str.contains('_').any())

        os.remove(output_file)
        os.remove(output_file.replace('.txt', '.log'))
