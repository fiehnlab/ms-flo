import glob
import logging
import os
import unittest

from msflo import parsers


LOG_FUNCTION = None


class TestMZmineParser(unittest.TestCase):
    def setUp(self):
        self.handler = parsers.get_handler('mzmine')


class TestParsers(unittest.TestCase):
    def setUp(self):
        self.mzmine_handler = parsers.get_handler('mzmine', LOG_FUNCTION)
        self.msdial_handler = parsers.get_handler('msdial', LOG_FUNCTION)
        self.xcms_handler = parsers.get_handler('xcms', LOG_FUNCTION)

    def tearDown(self):
        return

    def test_mzmine(self):
        logging.debug('Testing MZmine parser...')

        for f in glob.glob('test/data/mzmine/*.csv') + ['test/data/exports/MZmineExport.csv']:
            logging.debug('\tTesting %s' % f)

            self.assertTrue(self.mzmine_handler.validate_file(f))
            self.assertFalse(self.msdial_handler.validate_file(f))
            self.assertFalse(self.xcms_handler.validate_file(f))

            result = self.mzmine_handler.parse_file(f)
            self.assertIsNotNone(result)

            df, peak_columns = result

            self.assertIsNotNone(df)
            self.assertIsNotNone(peak_columns)
            self.assertGreater(len(df), 0)
            self.assertGreater(len(peak_columns), 0)

            self.assertIn('row ID', df.columns.values)

    def test_msdial(self):
        logging.debug('Testing MS-DIAL parser...')

        for f in glob.glob('test/data/msdial/*.txt') + ['test/data/exports/MSDial Export.txt']:
            logging.debug('\tTesting %s' % f)

            self.assertTrue(self.msdial_handler.validate_file(f))
            self.assertFalse(self.xcms_handler.validate_file(f))
            self.assertFalse(self.mzmine_handler.validate_file(f))

            result = self.msdial_handler.parse_file(f)
            self.assertIsNotNone(result)

            df, peak_columns = result

            self.assertIsNotNone(df)
            self.assertIsNotNone(peak_columns)
            self.assertGreater(len(df), 0)
            self.assertGreater(len(peak_columns), 0)

            self.assertIn('Alignment ID', df.columns.values)

    def test_msdial_empty_column(self):
        logging.debug('Testing MS-DIAL empty column handling...')

        f = 'test/20180205_empty_columns/empty_columns.txt'

        self.assertTrue(self.msdial_handler.validate_file(f))
        self.assertFalse(self.xcms_handler.validate_file(f))
        self.assertFalse(self.mzmine_handler.validate_file(f))

        result = self.msdial_handler.parse_file(f)
        self.assertIsNotNone(result)

        df, peak_columns = result

        self.assertIsNotNone(df)
        self.assertIsNotNone(peak_columns)
        self.assertGreater(len(df), 0)
        self.assertIs(len(df.columns), 39)
        self.assertIs(len(peak_columns), 10)

        self.assertIn('Alignment ID', df.columns.values)

    def test_xcms(self):
        logging.debug('Testing XCMS parser...')

        for f in glob.glob('test/data/xcms/*') + glob.glob('test/data/exports/*.xlsx'):
            logging.debug('\tTesting %s' % f)

            self.assertTrue(self.xcms_handler.validate_file(f))
            self.assertFalse(self.msdial_handler.validate_file(f))
            self.assertFalse(self.mzmine_handler.validate_file(f))

            result = self.xcms_handler.parse_file(f)
            self.assertIsNotNone(result)

            df, peak_columns = result

            self.assertIsNotNone(df)
            self.assertIsNotNone(peak_columns)
            self.assertGreater(len(df), 0)
            self.assertGreater(len(peak_columns), 0)

            self.assertIn('', df.columns.values)
