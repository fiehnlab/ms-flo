'use strict';

angular.module('app', ['ngAnimate', 'ngRoute', 'ui.bootstrap', 'ngFileUpload', 'angular-google-analytics'])
    /**
     *
     */
    .config(function($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/main.html'
            })
            .when('/submit', {
                templateUrl: 'views/submit.html',
                controller: 'SubmitController'
            })
            .when('/help', {
                templateUrl: 'views/help.html'
            })
            .when('/jobs/:id?', {
                templateUrl: 'views/job.html',
                controller: 'JobController'
            });
    })

    .config(['AnalyticsProvider', function (AnalyticsProvider) {
        AnalyticsProvider.setAccount('UA-87692241-3');
    }]).run(['Analytics', function(Analytics) {}])


    /**
     *
     */
    .controller('NavigationController', function($scope, $location) {
        $scope.navClass = function(page) {
            var currentRoute = $location.path().substring(1) || 'home';
            return page === currentRoute ? 'active' : '';
        };
    })


    /**
     *
     */
    .controller('SubmitController', function($scope, $http, $location, $window, CacheService, $filter, Analytics) {
        $scope.params = [
            {},                // Global parameters
            {enabled: true},   // Reference/contaminant ion removal
            {enabled: true},   // Duplicate removal
            {enabled: true},   // Isotope removal
            {enabled: true}    // Adduct joining
        ];

        $scope.upload_params = false;
        $scope.adducts = [{}];
        $scope.fileFormat = 'mzmine';
        $scope.ions = '';

        $scope.files = {};


        var ADDUCTS = [
            {name: 'M-H2O+H', difference: -17.00273, mode: 'Positive'},
            {name: 'M+H', difference: 1.007276, mode: 'Positive'},
            {name: 'M+NH4', difference: 18.033823, mode: 'Positive'},
            {name: 'M+Na', difference: 22.989218, mode: 'Positive'},
            {name: 'M+CH3OH+H', difference: 33.033489, mode: 'Positive'},
            {name: 'M+K', difference: 38.963158, mode: 'Positive'},
            {name: 'M+ACN+H', difference: 42.033823, mode: 'Positive'},

            {name: 'M-H2O-H', difference: -19.01839, mode: 'Negative'},
            {name: 'M-H', difference: -1.007276, mode: 'Negative'},
            {name: 'M+Cl', difference: 34.969402, mode: 'NegativeNegative'},
            {name: 'M+Formic Acid-H', difference: 44.998201, mode: 'Negative'},
            {name: 'M+Acetic Acid-H', difference: 59.013851, mode: 'Negative'},
            {name: 'M+Br', difference: 78.918885, mode: 'Negative'}
        ]


        $scope.filterAdducts = function(query) {
            var filtered = $filter('filter')(ADDUCTS, {name: query});
            filtered = JSON.parse(JSON.stringify(filtered));

            var foundPositive = false, foundNegative = false;

            for(var i = 0; i < filtered.length; i++) {
                filtered[i].formattedName = filtered[i].name
                    +' ('+ (filtered[i].difference > 0 ? '+': '') + filtered[i].difference +' m/z)';

                if(!foundPositive && filtered[i].mode == 'Positive') {
                    filtered[i].header = true;
                    foundPositive = true;
                }

                if(!foundNegative && filtered[i].mode == 'Negative') {
                    filtered[i].header = true;
                    foundNegative = true;
                }
            }

            return filtered;
        };

        $scope.onAdductSelect = function(adduct) {
            if(typeof(adduct['from adduct']) == 'object' && typeof(adduct['to adduct']) == 'object') {
                adduct['mass difference'] = adduct['to adduct'].difference - adduct['from adduct'].difference;
            }
        };


        $scope.addAdduct = function() {
            $scope.adducts.push({});
        };


        $scope.submitProcessingJob = function() {
            $scope.error = undefined;


            if(angular.isUndefined($scope.files.peak_file)) {
                $scope.newJobForm.peak_table_file.$setValidity('peak_table_file', false);
                $window.scrollTo(0, 0);
                return;
            }

            if($scope.upload_params && angular.isUndefined($scope.files.ini_file)) {
                $scope.newJobForm.peak_table_file.$setValidity('ini_file', false);
                $window.scrollTo(0, 0);
                return;
            }


            // Build form data
            var data = new FormData();
            data.append('peak_table', $scope.files.peak_file);
            data.append('file_format', $scope.fileFormat);


            if($scope.upload_params) {
                data.append('ini_file', $scope.files.ini_file);
            }

            else {
                // Format ions
                $scope.params[1].ion = [];

                if(angular.isDefined($scope.ions)) {
                    var ions = $scope.ions.split(',');

                    for(var i = 0; i < ions.length; i++) {
                        if(ions[i].trim() != '')
                            $scope.params[1].ion.push(parseFloat(ions[i]));
                    }
                }

                // Format adducts
                $scope.params[4].adduct = [];

                for(var i = 0; i < $scope.adducts.length; i++) {
                    var adduct = $scope.adducts[i];

                    if(angular.isDefined(adduct['from adduct']) && angular.isDefined(adduct['to adduct']) && angular.isDefined(adduct['mass difference'])) {
                        if (typeof(adduct['from adduct']) == 'object')
                            adduct['from adduct'] = adduct['from adduct'].name;

                        if (typeof(adduct['to adduct']) == 'object')
                            adduct['to adduct'] = adduct['to adduct'].name;

                        $scope.params[4].adduct.push($scope.adducts[i]);
                    }
                }

                data.append('parameters', angular.toJson($scope.params));
            }


            // POST data
            $scope.submittingJob = true;

            $http.post('/', data, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).then(
                function(response) {
                    // If data was processed successfully, redirect to download page
                    if(response.data.status == 'queued') {
                        // Track submission
                        Analytics.trackEvent('submit', 'success', response.data.id);

                        CacheService.setValue('job', response.data);
                        $location.path('jobs/'+ response.data.id);
                    }

                    // Otherwise, show form validation errors
                    else {
                        $scope.submittingJob = false;

                        // Track submission
                        Analytics.trackEvent('submit', 'error', response.data.error);

                        $scope.error = response.data.error;
                        $window.scrollTo(0, 0);
                    }
                },
                function(response) {
                    console.log('Error:')
                    console.log(response);

                    $scope.submittingJob = false;

                    // Track submission
                    Analytics.trackEvent('submit', 'error', JSON.stringify(response.data));

                    $scope.error = response.data;
                }
            );
        };
    })


    /**
     *
     */
    .controller('JobController', function($scope, $location, $routeParams, $timeout, $http, $window, CacheService, Analytics) {
        $scope.findJob = function(id) {
            $location.path('/jobs/'+ id)
        };

        $scope.refreshJobStatus = function() {
            $timeout(function() {
                $http.get('/jobs/'+ $routeParams.id)
                    .then(function(response) {
                        $scope.job = response.data;
                        $scope.scrollToBottom();

                        if($scope.job.status === 'queued' || $scope.job.status === 'running')
                            $scope.refreshJobStatus();
                        else if($scope.job.status === 'success' && $scope.auto_download) {
                            Analytics.trackEvent('job', 'download', $scope.job_id);
                            window.location.href = $scope.download_url;
                        }
                    });
            }, 2500);
        };

        $scope.scrollToBottom = function() {
            $timeout(function() {
                var height = Math.max(
                    document.body.scrollHeight,
                    document.body.offsetHeight,
                    document.documentElement.clientHeight,
                    document.documentElement.scrollHeight,
                    document.documentElement.offsetHeight
                );

                $window.scrollTo(0, height);
            }, 100);
        };

        (function() {
            // Add data to scope
            $scope.job = CacheService.getValue('job');

            if ($routeParams.id) {
                $scope.job_id = $routeParams.id;
                $scope.download_url = 'jobs/'+ $routeParams.id +'/download';

                // Set auto download if job is currently running
                if(angular.isDefined($scope.job) && ($scope.job.status === 'queued' || $scope.job.status === 'running'))
                    $scope.auto_download = true;

                $scope.refreshJobStatus();
            } else {
                $scope.enableSearch = true;
            }
        })();
    })


    /**
     *
     */
    .service('CacheService', function() {
        var cache = {};

        return {
            getValue: function(key) {
                if(cache.hasOwnProperty(key)) {
                    var value = cache[key];
                    delete cache[key];
                    return value;
                }
            },
            setValue: function(key, value) {
                cache[key] = value;
            }
        };
    })


    /**
     *
     */
    .directive('ngFileSelect', function () {
        return {
            link: function(scope, el) {
                el.bind('change', function(e) {
                    scope.$apply(function () {
                        scope.file = (e.srcElement || e.target).files[0];
                        scope.newJobForm.peak_table_file.$setValidity('peak_table_file', true);
                    });
                });
            }
        }
    })


    /**
     * Converts a string to title case (each word is capitalized)
     */
    .filter('titlecase', function() {
        return function(s) {
            s = ( s === undefined || s === null ) ? '' : s;

            return s.toString().toLowerCase().replace( /\b([a-z])/g, function(ch) {
                return ch.toUpperCase();
            });
        };
    });