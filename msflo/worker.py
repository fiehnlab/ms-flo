#!/usr/bin/env python3

import multiprocessing
import threading
import time


class Worker(threading.Thread):
    """Worker task for executing processing tasks"""
    
    def __init__(self, queue):
        threading.Thread.__init__(self)

        self.running = True
        self.queue = queue
    
    def run(self):
        while self.running:
            try:
                task_function, args = self.queue.get()

                p = multiprocessing.Process(target=task_function, args=args)
                p.start()
                p.join()

                self.queue.task_done()
            except Exception:
                pass
            
            time.sleep(1)

    def stop(self):
        this.running = False