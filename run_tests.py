#!/usr/bin/env python

from test.test_issues import *
from test.test_parsers import *

import argparse
import logging
import sys
import unittest


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('-v', '--verbose', action='store_true')
    args = parser.parse_args()

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG, format='%(asctime)-15s: %(message)s')
    
    unittest.main()