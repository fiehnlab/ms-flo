#!/usr/bin/env python


DEFAULT_PARAMETERS = """
[0. Global Parameters]
row merging delimiter = "_"

[1. Contaminant Ion Removal]
enabled = True
mz tolerance = 0.01

[2. Duplicate Removal]
enabled = True
mz tolerance = 0.01
retention time tolerance = 0.1
peak height tolerance = 1.0
minimum peak match ratio = 0.85

[3. Isotope Detection]
enabled = True
mz tolerance = 0.01
retention time tolerance = 0.02
minimum r^2 to match = 0.0
mass shift = 1.003355

[4. Adduct Joiner]
enabled = True
mz tolerance = 0.01
retention time tolerance = 0.02
"""


def unquote(s):
    quote_strings = ['"', '\\"', "'", "\\'"]
    s = s.strip()

    while s[0] == s[-1] or s[:2] == s[-2:]:
        for x in quote_strings:
            if s.startswith(x):
                s = s[len(x) : -len(x)].strip()
                break
        else:
            break

    return s.strip()



class ConfigHandler:
    CONFIG_SECTIONS = [
        '0. Global Parameters',
        '1. Contaminant Ion Removal',
        '2. Duplicate Removal',
        '3. Isotope Detection',
        '4. Adduct Joiner'
    ]

    DEFAULT_ADDUCT_FLAG_TOLERANCE = 0.0
    DEFAULT_ADDUCT_MERGE_TOLERANCE = 0.8
    

    def __init__(self):
        self.parser = SimpleConfigParser()
        self.writer = SimpleConfigWriter()

        self.DEFAULT_CONFIG = self.load_text(DEFAULT_PARAMETERS.splitlines(), False)

    def load_file(self, filename, fill_default_values = True):
        config = self.parser.parse_file(filename)
        return self.normalize_configuration_input(config, fill_default_values)

    def load_text(self, data, fill_default_values = True):
        config = self.parser.parse_data(data)
        return self.normalize_configuration_input(config, fill_default_values)

    def load_data(self, data, fill_default_values = True):
        return self.normalize_configuration_input(data, fill_default_values)

    def normalize_configuration_input(self, config, fill_default_values = True):
        # Convert dictionary type to list
        if type(config) == dict:
            config_dict, config = config, []

            for k, v in sorted(config_dict.items()):
                if k in ConfigHandler.CONFIG_SECTIONS:
                    idx = ConfigHandler.CONFIG_SECTIONS.index(k)

                    while len(config) <= idx:
                        config.append({})

                    config[idx] = v

        # Handle the standard configuration format
        if type(config) == list:
            # BUG FIX
            for idx in range(len(config)):
                for k in config[idx]:
                    if type(config[idx][k]) == str:
                        # Handle boolean
                        if config[idx][k].lower() == 'true' or config[idx][k].lower() == 'false':
                            config[idx][k] = (config[idx][k].lower() == 'true')

                        # Handle integer values
                        elif type(config[idx][k]) == str and self.parser.represents_int(config[idx][k]):
                            config[idx][k] = int(config[idx][k])

                        # Handle floating point values
                        elif type(config[idx][k]) == str and self.parser.represents_float(config[idx][k]):
                            config[idx][k] = float(config[idx][k])

            # Process adduct format
            idx = [i for i, x in enumerate(self.CONFIG_SECTIONS) if 'Adduct Joiner' in x][0]

            if 'adducts' in config[idx]:
                config[idx]['adduct'] = config[idx].pop('adducts')
            
            if 'adduct' in config[idx]:
                if type(config[idx]['adduct']) == str:
                    config[idx]['adduct'] = [config[idx]['adduct']]

                if type(config[idx]['adduct']) == list:
                    for i in range(len(config[idx]['adduct'])):
                        if type(config[idx]['adduct'][i]) == str:
                            config[idx]['adduct'][i] = [f(x) for f, x in zip((unquote, unquote, float, float, float), config[idx]['adduct'][i].lstrip('[').rstrip(']').split(','))]
                        
                        elif type(config[idx]['adduct'][i]) == dict:
                            config[idx]['adduct'][i]['mass difference'] = float(config[idx]['adduct'][i]['mass difference'])

                            if 'flag threshold' not in config[idx]['adduct'][i]:
                                config[idx]['adduct'][i]['flag threshold'] = self.DEFAULT_ADDUCT_FLAG_TOLERANCE
                            else:
                                config[idx]['adduct'][i]['flag threshold'] = float(config[idx]['adduct'][i]['flag threshold'])

                            if 'merge threshold' not in config[idx]['adduct'][i]:
                                config[idx]['adduct'][i]['merge threshold'] = self.DEFAULT_ADDUCT_MERGE_TOLERANCE
                            else:
                                config[idx]['adduct'][i]['merge threshold'] = float(config[idx]['adduct'][i]['merge threshold'])

                            config[idx]['adduct'][i] = [
                                unquote(config[idx]['adduct'][i]['from adduct']),
                                unquote(config[idx]['adduct'][i]['to adduct']),
                                config[idx]['adduct'][i]['mass difference'],
                                config[idx]['adduct'][i]['flag threshold'],
                                config[idx]['adduct'][i]['merge threshold']
                            ]
                else:
                    raise ValueError('Invalid `adduct` format: '+ config[idx]['adduct'])

            # Process ion format
            idx = [i for i, x in enumerate(self.CONFIG_SECTIONS) if 'Reference Ion Removal' in x or 'Contaminant Ion Removal' in x][0]

            if 'ions' in config[idx]:
                config[idx]['ion'] = config[idx].pop('ions')
            
            if 'ion' in config[idx]:
                if type(config[idx]['ion']) in [int, float, str]:
                    config[idx]['ion'] = [config[idx]['ion']]

                if type(config[idx]['ion']) == list:
                    config[idx]['ion'] = list(map(float, config[idx]['ion']))
                else:
                    raise ValueError('Invalid `ion` format: '+ config[idx]['ion'])

            # Fill in default values if missing
            if fill_default_values:
                for i in range(len(config)):
                    if len(config[i]) == 0 and i > 0:
                        config[i]['enabled'] = False
                    elif 'enabled' in config[i] and not config[i]['enabled']:
                        continue
                    else:
                        for k, v in self.DEFAULT_CONFIG[i].items():
                            if k not in config[i]:
                                config[i][k] = v

            return config

        # If configuration format is not recognized, throw a ValueError
        else:
            raise ValueError('Invalid configuration format')


    def export_config(self, filename, config):
        self.writer.write(filename, {self.CONFIG_SECTIONS[i] : config[i] for i in range(len(config))})




class SimpleConfigParser:
    """Based on http://nefaria.com/2012/08/simple-configuration-file-parser-python/"""

    INTEGER_REGEX = r'[+-]?\d+'
    FLOATING_POINT_REGEX = r'[+-]?'

    def __init__(self, comment_char = '#', option_char = '=', allow_duplicates = True, strip_quotes = True):
        self.comment_char = comment_char
        self.option_char = option_char
        self.allow_duplicates = allow_duplicates
        self.strip_quotes = True


    def represents_int(self, s):
        try:
            int(s)
            return True
        except ValueError:
            return False

    def represents_float(self, s):
        try:
            float(s)
            return True
        except ValueError:
            return False
 

    def parse_file(self, filename):
        with open(filename, 'r') as config_file:
            return self.parse_data(config_file)

    def parse_data(self, data):
        config = {}
        current_section = ''

        for line in data:
            line = line.strip()

            # Ignore comments
            if self.comment_char in line:
                line, comment = line.split(self.comment_char, 1)

            # Parse section heading
            if line.startswith('[') and line.endswith(']'):
                current_section = line.strip('[]').strip()

            # Parse configuration option
            elif self.option_char in line:
                # Separate option and value
                option, value = line.split(self.option_char, 1)
                option = option.strip().lower()
                value = value.strip()

                # Strip quotes
                if self.strip_quotes:
                    value = value.strip('"\'')


                # Handle boolean
                if value.lower() == 'true' or value.lower() == 'false':
                    value = (value.lower() == 'true')

                # Handle integer values
                elif self.represents_int(value):
                    value = int(value)

                # Handle floating point values
                elif self.represents_float(value):
                    value = float(value)


                # Create configuration section if needed
                if current_section not in config:
                    config[current_section] = {}

                # Store option entry
                if self.allow_duplicates:
                    if option in config[current_section]:
                        if type(config[current_section][option]) == list:
                            config[current_section][option].append(value)
                        else:
                            config[current_section][option] = [config[current_section][option], value]
                    else:
                        config[current_section][option] = value
                else:
                    config[current_section][option] = value
        
        return config




class SimpleConfigWriter:
    def __init__(self, comment_char = '#', option_char = '=', allow_duplicates = True):
        self.comment_char = comment_char
        self.option_char = option_char
        self.allow_duplicates = allow_duplicates

    def write(self, filename, config):
        with open(filename, 'w') as fout:
            for k in sorted(config):
                if k:
                    print('[%s]' % k, file = fout)

                if 'enabled' in config[k]:
                    print('enabled', self.option_char, config[k]['enabled'], file = fout)

                for option in config[k]:
                    if option == 'enabled':
                        continue
                    elif type(config[k][option]) == list:
                        for v in config[k][option]:
                             print(option, self.option_char, v, file = fout)
                    else:
                        print(option, self.option_char, config[k][option], file = fout)

                print('\n', file = fout)



if __name__ == '__main__':
    parser = SimpleConfigParser()
    writer = SimpleConfigWriter()
    handler = ConfigHandler()

    import pprint
    # pprint.pprint(parser.parse('default_parameters.ini'))
    # writer.write('test.ini', parser.parse('default_parameters.ini'))

    pprint.pprint(handler.load_file('example_parameters.ini'))