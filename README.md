# MS-FLO: Mass Spectral Feature List Optimizer 

[MS-FLO](http://msflo.fiehnlab.ucdavis.edu) is a tool to improve the quality of feature lists/peak tables after initial processing to expedite the process of data curation. It utilizes retention time alignments, accurate mass tolerances, Pearson’s correlation analysis, and peak height similarity to identify ion-adducts, duplicate peak reports and isotopic features of the main monoisotopic metabolites. Removing such erroneous peaks reduces the overall number of metabolites in data reports and improves the quality of subsequent statistical investigations.

MS-FLO was developed at the [Fiehn Research Lab](http://fiehnlab.ucdavis.edu/).

## Citation

If you utilize MS-FLO in your workflow, please consider citing:

DeFelice BC, Mehta SS, Samra S, Čajka T, Wancewicz B, Fahrmann JF, Fiehn O. Mass Spectral Feature List Optimizer (MS-FLO): a tool to minimize false positive peak reports in untargeted LC-MS data processing. Analytical Chemistry, February, 2017. [DOI: 10.1021/acs.analchem.6b04372](http://pubs.acs.org/doi/abs/10.1021/acs.analchem.6b04372)


## Usage

### Online version

MS-FLO is available online for public use: [http://msflo.fiehnlab.ucdavis.edu](http://msflo.fiehnlab.ucdavis.edu).  A peak table file is required, and parameters can either be specified individually or uploaded in a .ini configuration file.

### Command-line

A command-line runner is provided for integration into a local processing workflow.  MS-FLO requires Python 2.7.x or 3.3.x and its dependencies may be installed using `pip install -r requirements.txt`.  We recommend using a virtual environment with either virtualenv or Anaconda.

```
$ python run_msflo.py --help
usage: run_msflo.py [-h] [-a] [-f {mzmine,msdial,xcms}]
                    configuration_file peak_table_file [peak_table_file ...]

positional arguments:
  configuration_file    ini configuration file
  peak_table_file       peak table export files

optional arguments:
  -h, --help            show this help message and exit
  -a, --archive         compress generated files (default: False)
  -f {mzmine,msdial,xcms}, --format {mzmine,msdial,xcms}
```

A peak table file and .ini configuration file are required for processing by the command line and a file format must also be specified.

### Local web deployment

The MS-FLO web interface can also be run locally after installing the Python dependencies as described in the command-line section: `python run_web_app.py`.  By default, the web service is accessible at http://localhost:8080.  

### Docker deployment

A Docker build script is also provided which packages the static web dependencies and builds a runnable Docker image.  This image can be run locally 

```
$ cd docker
$ ./docker_build.sh
$ docker run -it -p 8080:8080 msflo:latest
```

## Contributing

Feedback and contributions are very welcome!  Please use the issue tracker to submit any suggestions or encountered problems.

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.