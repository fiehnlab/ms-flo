#!/usr/bin/env python

import argparse
import logging

from msflo import msflo_processor


#
# Configure logging
#
logging.basicConfig(level=logging.INFO, format='%(asctime)-15s: %(message)s')
logger = logging.getLogger()
logger.setLevel(logging.INFO)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('configuration_file', help='ini configuration file')
    parser.add_argument('peak_table_file', nargs='+', help='peak table export files')
    parser.add_argument('-a', '--archive', action='store_true', help='compress generated files (default: %(default)s)')
    parser.add_argument('-f', '--format', choices=['mzmine', 'msdial', 'xcms'], required=True)
    args = parser.parse_args()

    for peak_table_file in args.peak_table_file:
        msflo_processor.run_msflo(args.configuration_file, peak_table_file, args.format, args.archive)