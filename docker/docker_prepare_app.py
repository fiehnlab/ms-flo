#!/usr/bin/env python

from __future__ import print_function
import bs4
import glob
import hashlib
import os
import pathlib
import shutil


def md5sum(data):
    return hashlib.md5(data.encode('utf-8')).hexdigest()

def add_filename_hashes(html, pattern):
	p = pathlib.Path('target')

	for f in p.glob('**/'+ pattern):
		print('Hashing %s...' % f.relative_to(p))

		# Rename file
		data = f.read_text()
		new_f = f.parent / (md5sum(data)[:8] +'.'+ f.name)
		print(new_f)

		f.rename(new_f)

		# Update filename in html
		html = html.replace(str(f.relative_to(p)), str(new_f.relative_to(p)))

	return html


def concatenate_vendor_js(html):
	print('Concatenating vendor javascript...')

	# Find all used bower javascript scripts
	soup = bs4.BeautifulSoup(html, 'lxml')
	scripts = [x for x in soup.findAll('script') if 'bower_components' in x.attrs['src']]

	# Concatenate javascript scripts
	concatenated_js = ''

	for x in scripts:
		with open('../static/'+ x.attrs['src']) as f:
			print('\tCopying %s...' % x.attrs['src'])
			concatenated_js += f.read()

	# Write concatenated vendor file
	filename = 'js/%s.vendor.js' % md5sum(concatenated_js)[:8]

	with open('target/%s' % filename, 'w') as fout:
		print('\tWriting concatenated vendor file')
		print(concatenated_js, file = fout)

	# Update html script references
	scripts[0].attrs['src'] = filename

	for x in scripts[1:]:
		x.decompose()

	return str(soup).replace('\n', '')


def copy_vendor_css(html):
	print('Copying vendor css...')

	# Find all used bower css files
	soup = bs4.BeautifulSoup(html, 'lxml')

	for x in soup.findAll('link'):
		if 'bower_components' in x.attrs['href']:
			print('\tCopying %s...' % x.attrs['href'])

			shutil.copy('../static/'+ x.attrs['href'], 'target/styles/')
			x.attrs['href'] = 'styles/'+ x.attrs['href'].split('/')[-1]

	return str(soup).replace('\n', '')

def copy_fonts():
	# Make fonts directory if it does not exist
	if not os.path.isdir('target/fonts'):
		os.mkdir('target/fonts')

	for f in glob.glob('../static/bower_components/*/fonts/*'):
		print('\tCopying %s...' % f)
		shutil.copy(f, 'target/fonts/')


if __name__ == '__main__':
	# Delete target directory if it exists
	if os.path.isdir('target'):
		print('Removing target directory...')
		shutil.rmtree('target')

	# Copy directory tree
	print('Copying static directory to target...')
	shutil.copytree('../static', 'target', ignore=shutil.ignore_patterns('bower_components'))

	# Read index.html
	with open('target/index.html') as f:
		html = f.read()

	# Add filename hashes
	html = add_filename_hashes(html, '*.js')
	html = add_filename_hashes(html, '*.css')

	# Concatenate bower javascript
	html = concatenate_vendor_js(html)

	# Copy bower css
	html = copy_vendor_css(html)

	# Copy fonts
	print('Copying fonts...')
	copy_fonts()

	# Export html
	print('Exporting updated index.html...')
	with open('target/index.html', 'w') as fout:
		print(html, file = fout)

	print('Done!')
