#!/usr/bin/env python

import argparse
import collections
import datetime
import glob
import io
import logging
import numpy as np
import os
import pandas as pd
import re
import sys
import time
import traceback
import zipfile

from mongoengine import connect, BooleanField, DateTimeField, Document, FloatField, IntField, StringField

from . import parsers
from . import parameters


#
# Configure logging
#
logger = logging.getLogger('msflo')
logger.setLevel(logging.INFO)



class MSFLOProcessor(Document):
    ION_STR = '#%d (%.2f_%.2f)'
    MAX_ISOTOPE = 3


    #
    # ORM  fields and methods
    #
    job_id = StringField(required=True, primary_key=True)
    filename = StringField(required=True)
    basename = StringField(required=True)
    file_format = StringField(required=True)

    job_log = StringField(required=True)
    job_log_length = IntField(required=True)

    error = StringField()
    queued = BooleanField(required=True)
    running = BooleanField(required=True)

    start_time = DateTimeField()
    runtime = FloatField()

    meta = {'collection': 'msflo'}

    def save(self, *args, **kwargs):
        log = self.get_log().splitlines()
        self.job_log = '\n'.join(log[-250:])
        self.job_log_length = len(log)
        return super(MSFLOProcessor, self).save(*args, **kwargs)


    #
    # Magic Methods
    # 

    def __init__(self, job_id, filename, file_format, quiet=True, mongo=False, mongo_host=None, mongo_db=None, *args, **kwargs):
        super(Document, self).__init__(*args, **kwargs)

        self.job_id = job_id
        self.filename = filename
        self.mongo = mongo

        if filename.endswith('.csv') or filename.endswith('.txt') or filename.endswith('.xlsx'):
            self.basename = filename.rsplit('.', 1)[0]
        else:
            self.basename = filename

        self.error = None
        self.queued = True
        self.running = False

        # Initialize logging
        self._log = io.StringIO()
        self._logger = logging.getLogger(job_id)
        self._last_log = time.time()
        # self._logger.setLevel(logging.INFO)

        # Setup the console handler with a StringIO object
        # http://alanwsmith.com/capturing-python-log-output-in-a-variable
        self._log_handler = logging.StreamHandler(self._log)
        self._log_handler.setLevel(logging.DEBUG)
        self._log_handler.setFormatter(logging.Formatter('%(asctime)s: %(message)s'))
        self._logger.addHandler(self._log_handler)

        # Set parser
        self.file_format = file_format
        self.parser = parsers.get_handler(self.file_format, self.get_logger())

        self.ID_COL = self.parser.ID_COLUMN
        self.MZ_COL = self.parser.REQUIRED_COLUMNS['mz']
        self.RT_COL = self.parser.REQUIRED_COLUMNS['rt']

        # Save job if database persistece is enabled
        if mongo:
            self.mongo_host = mongo_host
            self.mongo_db = mongo_db
            self.save()

    def __del__(self):
        self._log.close()
        self._logger.removeHandler(self._log_handler)



    #
    # Logging methods
    #

    def get_logger(self):
        return self._logger.info

    def log(self, msg):
        self._logger.info(msg)

        # Update database at most every 5 seconds
        if self.mongo and time.time() - self._last_log > 5:
            self._last_log += 1
            self.save()

    def debug(self, msg):
        self.log(msg)



    #
    # Runtime methods
    #

    def get_log(self):
        return self._log.getvalue()

    def get_error(self):
        return self.error


    def is_queued(self):
        return self.queued

    def is_running(self):
        return self.running and self.error is None

    def is_error(self):
        return self.error is not None

    def is_finished(self):
        return not self.queued and not self.running and self.error is None

    def get_status(self):
        if self.is_error():
            return 'error'
        elif self.is_queued():
            return 'queued'
        elif self.is_running():
            return 'running'
        elif self.is_finished():
            return 'finished'
        else:
            return 'unknown'


    def start(self, df, peak_columns, config):
        self.log('Starting processing task...')

        self.df = df
        self.peak_columns = peak_columns
        self.config = config

        self.queued = False
        self.running = True

        self.start_time = datetime.datetime.now()

        if self.mongo:
            self.save()

    def stop(self):
        self.log('Done!')

        self.running = False
        self.runtime = (datetime.datetime.now() - self.start_time).total_seconds()

        if self.mongo:
            self.save()

    def load(self, suffix='processed'):
        self.queued = False
        self.running = False

        with open('%s_%s.log' % (self.basename, suffix)) as f:
            self.log(f.read())



    #
    # I/O methods
    #


    def export_log(self, suffix='processed'):
        # Export log file
        filename = self.basename +'_' + suffix + '.log'

        with open(filename, 'wb') as f:
            f.write(self.get_log().encode('utf-8'))
        
        self.log('Wrote to %s' % filename)


    def archive_results(self):
        self.log('Archiving...')

        # Get files to archive
        files = glob.glob(self.basename +'*')

        # Create ZIP archive
        archive = zipfile.ZipFile(self.basename +'.zip', 'w', zipfile.ZIP_DEFLATED)

        for f in files:
            archive.write(f, arcname = f.split('/')[-1])

            if not f.endswith('.log'):
                os.remove(f)

        archive.close()


    #
    # Analysis Methods
    #

    def row_correlation(self, i, j):
        x = np.array([self.df[col][i] for col in self.peak_columns])
        y = np.array([self.df[col][j] for col in self.peak_columns])

        # Find 2 sigma outliers
        # outliers = (abs(x - np.mean(x)) > 2 * np.std(x)) | (abs(y - np.mean(y)) > 2 * np.std(y))
        # x, y = x[~outliers], y[~outliers]

        if all(k < 1.0e-18 for k in x) or all(k < 1.0e-18 for k in y):
            return 0
        else:
            # http://stackoverflow.com/a/895063
            p = np.poly1d(np.polyfit(x, y, 1))

            yhat = p(x)
            ybar = np.mean(y)
            ssreg = np.sum((yhat - ybar)**2)
            sstot = np.sum((y - ybar)**2)

            return ssreg / sstot



    #
    # Processing methods
    #

    def merge_changes(self):
        # Get the names of all peak height columns
        self.peak_columns = [s for s in self.df.columns.values if s.endswith('.d')]
        self.log('\tIdentified %d peak height columns' % len(self.peak_columns))

        if 'ID' in self.df.columns.values:
            self.df.loc[self.df.ID.isnull(), 'ID'] = 'NOID'
        if 'duplicate_flag' in self.df.columns.values:
            self.df.loc[self.df.duplicate_flag.isnull(), 'duplicate_flag'] = ''
        if 'adduct_flag' in self.df.columns.values:
            self.df.loc[self.df.adduct_flag.isnull(), 'adduct_flag'] = ''
        if 'isotope_flag' in self.df.columns.values:
            self.df.loc[self.df.isotope_flag.isnull(), 'isotope_flag'] = ''



        # Regex to find adducts to join
        adduct_regex = re.compile(r'Join\w* #(\d{3}).*(\[M[+-]\w+\]) -> (\[M[+-]\w+\]) \((\[M\+H\+\d\])\) w/ R\^2 = (\d+\.\d+)', re.IGNORECASE)

        matches = []

        if 'adduct_flag' in self.df.columns.values:
            self.log('Analyzing adduct flags...')

            data = self.df[~self.df.adduct_flag.isnull()].sort('adduct_flag')

            for i in data.index:
                result = adduct_regex.findall(data.adduct_flag[i])

                if result:
                    matches.append((i, result[0]))
        else:
            self.log('No adduct flag found')

        for i, (match_id, a1, a2, identifier, R) in matches:
            # Find matching row
            data = self.df[~self.df.adduct_flag.isnull()]
            data = data[data.adduct_flag.str.contains('#%s' % match_id)]

            match_regex = re.compile(r'#%s: (\[M[+-]\w+\]) -> (\[M[+-]\w+\]) \((\[M\+H\+\d\])\) w/ R\^2 = (\d+\.\d+)' % match_id, re.IGNORECASE)
            flag = ''

            for j in data.index:
                match = match_regex.findall(data.adduct_flag[j])

                if j != i:
                    # Log match
                    self.log('\tJoining Adduct #%s (%s and %s)' % (match_id, self.df.identifier[i], identifier))

                    # Sum peak heights
                    if self.SUM_WATER_LOSS or 'H2O' not in a1 + a2:
                        for col in self.peak_columns:
                            self.df[col][i] += self.df[col][j]
                    else:
                        if 'H2O' in a2 and config['row merging delimiter'] not in self.df[self.ID_COL][j]:
                            pass

                        elif 'H2O' in a1 and config['row merging delimiter'] not in self.df[self.ID_COL][i]:
                            for col in self.peak_columns:
                                self.df[col][i] = self.df[col][j]
                                
                        else:
                            for col in self.peak_columns:
                                self.df[col][i] += self.df[col][j]

                    # Join row information
                    if 'ID' in self.df.columns.values:
                        self.df['ID'][i] += config['row merging delimiter'] + self.df['ID'][j]
                    self.df['identifier'][i] += config['row merging delimiter'] + self.df['identifier'][j]
                    self.df[self.ID_COL][i] += config['row merging delimiter'] + self.df[self.ID_COL][j]
                    self.df[self.MZ_COL][i] += config['row merging delimiter'] + self.df[self.MZ_COL][j]
                    self.df[self.RT_COL][i] += config['row merging delimiter'] + self.df[self.RT_COL][j]

                    if 'row number of detected peaks' in self.df.columns.values:
                        self.df['row number of detected peaks'][i] += config['row merging delimiter'] + self.df['row number of detected peaks'][j]

                    if 'duplicate_flag' in self.df.columns.values and data.duplicate_flag[j]:
                        self.df.duplicate_flag[i] += ('' if self.df.duplicate_flag[i] == '' else ' | ') + self.df.duplicate_flag[j]
                    
                    if 'isotope_flag' in self.df.columns.values and data.isotope_flag[j]:
                        self.df.isotope_flag[i] += ('' if self.df.isotope_flag[i] == '' else ' | ') + self.df.isotope_flag[j]
                
                    # Drop matched row
                    self.df.drop(j, inplace = True)

                # Update flag
                for m in match:
                    if flag != '': flag += ' | '
                    flag += 'Matched %s to %s (%s) w/ R^2 = %s' % m

            self.df.adduct_flag[i] = flag


        # Regex to find isotope to join
        isotope_regex = re.compile(r'Join\w* #(\d{3}).* (\[M\+H\+\d\]) = (\[M\+H\+\d\]) w/ R\^2 = (\d+\.\d+), dRT = (\d+\.\d{3}), PHR = (\d+\.\d{3}) \+/- (\d+\.\d{3})', re.IGNORECASE)

        matches = []

        if 'isotope_flag' in self.df.columns.values:
            self.log('Analyzing isotope flags...')

            data = self.df[~self.df.isotope_flag.isnull()].sort('isotope_flag')

            for i in data.index:
                result = isotope_regex.findall(data.isotope_flag[i])

                if result:
                    matches.append((i, result[0]))
        else:
            self.log('No isotope flag found')

        for i, (match_id, identifier, isotope, R, dRT, PHR, sPHR) in matches:
            # Find matching row
            data = self.df[~self.df.isotope_flag.isnull()]
            data = data[data.isotope_flag.str.contains('#%s' % match_id)]

            match_regex = re.compile(r'#%s: ([\d\._]+) = (\[M\+H\+\d\]) w/ R\^2 = (\d+\.\d+), dRT = (\d+\.\d+), PHR = (\d+\.\d+) \+/- (\d+\.\d+)' % match_id, re.IGNORECASE)
            flag = ''

            for j in data.index:
                match = match_regex.findall(data.isotope_flag[j])

                if j != i:
                    # Log match
                    self.log('\tJoining Isotope #%s (%s and %s)' % (match_id, self.df.identifier[i], identifier))

                    # Sum peak heights
                    if self.SUM_WATER_LOSS or 'H2O' not in a1 + a2:
                        for col in self.peak_columns:
                            self.df[col][i] += self.df[col][j]
                    else:
                        if 'H2O' in a2 and config['row merging delimiter'] not in self.df[self.ID_COL][j]:
                            pass

                        elif 'H2O' in a1 and config['row merging delimiter'] not in self.df[self.ID_COL][i]:
                            for col in self.peak_columns:
                                self.df[col][i] = self.df[col][j]
                                
                        else:
                            for col in self.peak_columns:
                                self.df[col][i] += self.df[col][j]

                    # Join row information
                    if 'ID' in self.df.columns.values:
                        self.df['ID'][i] += config['row merging delimiter'] + self.df['ID'][j]
                    self.df['identifier'][i] += config['row merging delimiter'] + self.df['identifier'][j]
                    self.df[self.ID_COL][i] += config['row merging delimiter'] + self.df[self.ID_COL][j]
                    self.df[self.MZ_COL][i] += config['row merging delimiter'] + self.df[self.MZ_COL][j]
                    self.df[self.RT_COL][i] += config['row merging delimiter'] + self.df[self.RT_COL][j]

                    if 'row number of detected peaks' in self.df.columns.values:
                        self.df['row number of detected peaks'][i] += config['row merging delimiter'] + self.df['row number of detected peaks'][j]

                    if 'duplicate_flag' in self.df.columns.values and data.duplicate_flag[j]:
                        self.df.duplicate_flag[i] += ('' if self.df.duplicate_flag[i] == '' else ' | ') + self.df.duplicate_flag[j]
                    
                    if 'adduct_flag' in self.df.columns.values and data.adduct_flag[j]:
                        self.df.adduct_flag[i] += ('' if self.df.adduct_flag[i] == '' else ' | ') + self.df.adduct_flag[j]

                    # Drop matched row
                    self.df.drop(j, inplace = True)

                # Update flag
                for m in match:
                    if flag != '': flag += ' | '
                    flag += 'Joined isotope %s (%s) w/ R^2 = %s, dRT = %s, PHR = %s +/- %s' % m

            self.df.isotope_flag[i] = flag


    def remove_reference_ions(self):
        # Get configuration
        config = self.config[1].copy()
        config.update(self.config[0])

        if not config['enabled'] or 'ion' not in config or len(config['ion']) == 0:
            return

        self.log('Removing contaminant ions...')

        # Find matches
        for mz in config['ion']:
            x = self.df[abs(self.df.mz - mz) < config['mz tolerance']]

            # Log matches
            for i in x.index:
                self.log('Found contaminant ion: '+ self.ION_STR % (self.df.id[i], self.df.rt[i], self.df.mz[i]))

            # Remove reference ions
            self.df.drop(x.index, inplace = True)

        self.log('Reduced to %d peaks' % len(self.df))


    def remove_duplicates(self):
        # Get configuration
        config = self.config[2].copy()
        config.update(self.config[0])

        if not config['enabled']:
            return

        self.log('Performing duplicate removal...')

        # Sort by m/z
        self.df.sort_values(by = 'mz', inplace = True)

        # Indices to keep and remove
        keep = []
        flag = {}
        remove = {}

        # Iterate over all rows of the data frame to find duplicates
        for idx, row in self.df.iterrows():

            # Find matches of this row
            x = self.df[abs(self.df.mz - row.mz) < config['mz tolerance']]
            x = x[x.mz > row.mz]
            x = x[abs(x.rt - row.rt) < config['retention time tolerance']]


            # Compare the peak heights to determine match
            for match_idx, match in x.iterrows():
                # Count the number/ratio of peaks that match within the peak height tolerance level
                peak_matches = sum(abs(row[col] - match[col]) < config['peak height tolerance'] for col in self.peak_columns)
                ratio = 1.0 * peak_matches / len(self.peak_columns)

                if ratio < config['minimum peak match ratio']:
                    if idx in keep or idx in flag:
                        flag[match_idx] = (idx, ratio)
                    elif idx in remove:
                        # If this row is to be removed, compare the match with its duplicate
                        i = remove[idx]

                        peak_matches = sum(abs(self.df[col][i] - match[col]) < config['peak height tolerance']  for col in self.peak_columns)
                        ratio = 1.0 * peak_matches / len(self.peak_columns)

                        if ratio < config['minimum peak match ratio']:
                            flag[match_idx] = (i, ratio)
                        else:
                            remove[match_idx] = i
                            self.df.loc[i, 'id_annotation'] += config['row merging delimiter'] + self.df.loc[match_idx, 'id_annotation']
                    else:
                        #flag[idx] = match_idx
                        flag[match_idx] = (idx, ratio)

                else:
                    if idx in keep or idx in remove:
                        remove[match_idx] = idx
                        self.df.loc[idx, 'id_annotation'] += config['row merging delimiter'] + self.df.loc[match_idx, 'id_annotation']
                    elif idx in flag:
                        flag[match_idx] = (idx, ratio)
                    else:
                        keep.append(idx)
                        remove[match_idx] = idx
                        self.df.loc[idx, 'id_annotation'] += config['row merging delimiter'] + self.df.loc[match_idx, 'id_annotation']


                # Print match information
                data = (row.id, row.rt, row.mz, match.id, match.rt, match.mz)
                status = 'FLAGGED' if ratio < config['minimum peak match ratio'] else 'DUPLICATE'

                self.log(('Found duplicate '+ self.ION_STR +' and '+ self.ION_STR) % data)
                self.log('\t-->  %s - %d / %d (%.3f)' % (status, peak_matches, len(self.peak_columns), ratio))
        

        # Add flag comments
        for i, (k, (j, r)) in enumerate(flag.items()):
            flag = 'Match #%03d: Possible duplicate of %s (%.1f%%)' % (i + 1, self.df.identifier[j], 100 * r)
            self.df.loc[k, 'duplicate_flag'] += ('' if self.df.duplicate_flag[k] == '' else ' | ') + flag
            self.df.loc[j, 'duplicate_flag'] += ('' if self.df.duplicate_flag[j] == '' else ' | ') + 'Match #%03d' % (i + 1)

        # Drop duplicatees
        self.df.drop(remove, inplace = True)
        self.log('Reduced to %d peaks' % len(self.df))


    def known_adduct_joiner(self):
        # Get configuration
        config = self.config[4].copy()
        config.update(self.config[0])

        if not config['enabled'] or 'adduct' not in config or len(config['adduct']) == 0:
            return


        if 'ID' in self.df.columns.values:
            self.log('Performing adduct joiner on identified peaks...')
        else:
            self.log('Skipping adduct joiner on identified peaks')
            return

        # Sort data by retention time
        self.df.sort('rt', inplace = True)

        # Define an adduct string
        is_adduct = lambda s: s[0] == '[' and s[-2] == ']' and s[-1] in ('+', '-')

        # Find the data frame indices of all identified peaks
        idx = self.df[~self.df.ID.isnull()].index


        # Combine peaks by name
        #   - key: compound name
        #   - value: list of tuples (adduct name, row ID)
        names = collections.defaultdict(list)

        for i in idx:
            x = self.df.ID[i].split()

            if len(x) > 1 and is_adduct(x[-1]):
                names[' '.join(x[: -1])].append((x[-1], i))


        # Remove names with only one adduct
        names = {k: v for k, v in names.items() if len(v) > 1}


        # Keep a track of the adducts that were joined
        #   - key: tuple (compound name, joined row ID)
        #   - value: list of tuples ()
        joined = {}


        # Combine adducts
        for k, v in names.items():
            # Sort dictionary values by retention time
            v = [x[1] for x in sorted((self.df.rt[x[1]], x) for x in v)]

            # Join successive adducts
            i = 0

            for j in range(1, len(v)):
                # Retention time of this row
                rt = self.df['rt'][v[j][1]]

                # Retention times of potentially joined adducts
                rts = map(float, self.df[self.RT_COL][v[i][1]].split(config['row merging delimiter']))
                
                # If this row's retention time matches the current row's, join them
                if any(abs(t - rt) < config['retention time tolerance'] for t in rts):
                    key = (k, self.df.id[v[i][1]])

                    # Store adduct information
                    if key not in joined:
                        joined[key] = [(v[i][0], self.df.id[v[i][1]], self.df.rt[v[i][1]], self.df.mz[v[i][1]])]
                    joined[key].append((v[j][0], self.df.id[v[j][1]], self.df.rt[v[j][1]], self.df.mz[v[j][1]]))

                    # Sum peak heights
                    # TODO: Revisit this feature
                    if False and (self.SUM_WATER_LOSS or 'H2O' not in self.df.ID[v[i][1]] + self.df.ID[v[j][1]]):
                        for col in self.peak_columns:
                            self.df.loc[v[i][1], col] += self.df[col][v[j][1]]
                    else:
                        if 'H2O' in self.df.ID[v[j][1]]:
                            pass
                        
                        elif 'H2O' in self.df.ID[v[i][1]] and config['row merging delimiter'] not in self.df[self.ID_COL][v[i][1]]:
                            for col in self.peak_columns:
                                self.df.loc[v[i][1], col] = self.df[col][v[j][1]]
                        
                        else:
                            for col in self.peak_columns:
                                self.df.loc[v[i][1], col] += self.df[col][v[j][1]]

                    # Join row information
                    metadata_columns = [c for c in self.df.columns.values if c not in self.peak_columns and c not in ['id', 'mz', 'rt', 'adduct_flag']]

                for col in metadata_columns:
                    delimiter = ' | ' if '_flag' in col else config['row merging delimiter']

                    # Only merge if
                    #   1) Merged value is not null
                    #   2) Merged value is different
                    #   3) Merged value is not very long and is not a flag value
                    if self.df.loc[v[j][1], col] and self.df.loc[v[i][1], col] != self.df.loc[v[j][1], col] and \
                            ('_flag' in col or len(str(self.df.loc[v[j][1], col])) < 100):
                        self.df.loc[v[i][1], col] += ('' if str(self.df.loc[v[i][1], col]) == '' else delimiter) + str(self.df.loc[v[j][1], col])

                # Otherwise update the current row
                # else:
                #     i = j

                # Drop matched row
                self.df.drop(v[j][1], inplace = True)

        # Print match information
        for k, v in joined.items():
            self.log('Found identified adducts for %s:' % k[0])

            for x in v:
                self.log(('\t-->  %s at '+ self.ION_STR) % x)

        self.log('Reduced to %d peaks' % len(self.df))


    def isotope_search(self):
        # Get configuration
        config = self.config[3].copy()
        config.update(self.config[0])

        if not config['enabled']:
            return


        self.log('Performing isotope search...')

        # Sort data by retention time
        self.df.sort_values(by = 'mz', inplace = True)

        # Keep a track of identified isotopes
        identified = []

        count = 0

        # Iterate to search for up to 
        for i in self.df.index:
            # Skip if this has been identified as an isotope peak
            if i in identified:
                continue

            mz = self.df.mz[i]
            rt = self.df.rt[i]

            log = []

            for n in range(1, self.MAX_ISOTOPE + 1):
                m = self.df[(abs(self.df.mz - mz - n * config['mass shift']) < config['mz tolerance']) & 
                        (abs(self.df.rt - rt) < config['retention time tolerance'])]

                for j in m.index:
                    # Mark as identified
                    identified.append(j)
                    R = self.row_correlation(i, j)

                    if R < config['minimum r^2 to match']:
                        continue

                    # Compute peak height ratio for the isotope
                    m0_found = (0 not in [self.df[col][i] for col in self.peak_columns])
                    ratios = [1.0 * self.df[col][j] / self.df[col][i] for col in self.peak_columns if self.df[col][i] > 0 and self.df[col][j] > 0]
                    PHR, PHS = np.mean(ratios), np.std(ratios)

                    # Write flags
                    self.df.loc[i, 'isotope_flag'] += ('' if self.df.isotope_flag[i] == '' else ' | ') + \
                            'Match #%03d: %s = [M+H+%d] w/ R^2 = %.3f, dRT = %.3f, PHR = %.3f +/- %.3f' \
                            % (count + 1, self.df.identifier[j], n, R, abs(self.df.rt[j] - rt), PHR, PHS)
                    self.df.loc[j, 'isotope_flag'] += ('' if self.df.isotope_flag[j] == '' else ' | ') + \
                            'Match #%03d' % (count + 1)

                    if n == 1 or self.df.isotope_phr[i] == '':
                        self.df.loc[i, 'isotope_phr'] = PHR

                    # Add to log
                    log.append(('\t--> [M+H+%d] '+ self.ION_STR +' w/ R^2 = %.3f, dRT = %.3f, PHR = %.3f') % (n, m.id[j], m.rt[j], m.mz[j], R, abs(self.df.rt[j] - rt), PHR))
                    count += 1


            # Write to log
            if len(log) > 0:
                self.log(('Isotope(s) identified for '+ self.ION_STR +':') % (self.df.id[i], rt, mz))

                for s in log:
                    self.log(s)


        self.log('Found %d isotope peaks' % count)


    def unknown_adduct_joiner(self):
        # Get configuration
        config = self.config[4].copy()
        config.update(self.config[0])

        if not config['enabled'] or 'adduct' not in config or len(config['adduct']) == 0:
            return


        self.log('Performing adduct joiner on unknown peaks...')

        # Sort data by retention time
        self.df.sort_values(by = 'mz', inplace = True)


        # Indices to keep and remove
        matches = {}
        flags = {}

        # Count matches and flags
        count_matches = collections.defaultdict(int)
        count_flags = collections.defaultdict(int)


        for i in self.df.index:
            mz = self.df.mz[i]
            rt = self.df.rt[i]

            x = self.df[(abs(self.df.rt - rt) < config['retention time tolerance']) & (self.df.mz - mz > 0)]

            for a, (a1, a2, diff, minR, threshR) in enumerate(config['adduct']):
                m = x[abs(x.mz - self.df.mz[i] - diff) < config['mz tolerance']]

                for j in m.index:
                    R = self.row_correlation(i, j)

                    # Store a match if it meets the minimum R^2 value
                    if R >= threshR:
                        matches[(i, j)] = (a1, a2, R)
                        count_matches[i] += 1
                        count_matches[j] += 1
                    elif R >= minR:
                        flags[(i, j)] = (a1, a2, R)
                        count_flags[i] += 1
                        count_flags[j] += 1

                    self.log('Found adduct match with R^2 = %.3f:' % R)
                    self.log(('\t-->  %s - '+ self.ION_STR) % (a1, self.df.id[i], self.df.rt[i], self.df.mz[i]))
                    self.log(('\t-->  %s - '+ self.ION_STR) % (a2, self.df.id[j], self.df.rt[j], self.df.mz[j]))

        
        for k in list(matches.keys()):
            if sum(count_matches[i] + count_flags[i] for i in k) == 2:
                i, j = k
                a1, a2, R = matches[k]

                # Sum peak heights
                if False and (self.SUM_WATER_LOSS or 'H2O' not in a1 + a2):
                    for col in self.peak_columns:
                        self.df[col][i] += self.df[col][j]
                else:
                    if 'H2O' in a2 and config['row merging delimiter'] not in self.df[self.ID_COL][j]:
                        pass

                    elif 'H2O' in a1 and config['row merging delimiter'] not in self.df[self.ID_COL][i]:
                        for col in self.peak_columns:
                            self.df.loc[i, col] = self.df.loc[j, col]
                            
                    else:
                        for col in self.peak_columns:
                            self.df.loc[i, col] += self.df.loc[j, col]

                # Join row information
                metadata_columns = [c for c in self.df.columns.values if c not in self.peak_columns and c not in ['id', 'mz', 'rt', 'adduct_flag']]

                for col in metadata_columns:
                    delimiter = ' | ' if '_flag' in col else config['row merging delimiter']

                    # Only merge if
                    #   1) Merged value is not null
                    #   2) Merged value is different (and is not the ID, m/z or RT)
                    #   3) Merged value is not very long and is not a flag value

                    if not self.df.loc[j, col]:
                        continue
                    if str(self.df.loc[i, col]) == str(self.df.loc[j, col]) and col not in [self.parser.ID_COLUMN, self.parser.REQUIRED_COLUMNS['mz'], self.parser.REQUIRED_COLUMNS['rt']]:
                        continue
                    if '_flag' not in col and len(str(self.df.loc[j, col])) >= 100:
                        continue

                    self.df.loc[i, col] = str(self.df.loc[i, col]) + ('' if str(self.df.loc[i, col]) == '' else delimiter) + str(self.df.loc[j, col])


                # Update adduct flag
                self.df.loc[i, 'adduct_flag'] += ('' if self.df.loc[i, 'adduct_flag'] == '' else ' | ') + \
                        'Matched %s to %s (%s) w/ R^2 = %.3f' % (a1, a2, self.df.loc[j, 'identifier'], R)

                # Drop matched row
                self.df.drop(j, inplace = True)
                del matches[k]

        self.log('Reduced to %d peaks' % len(self.df))


        # Add flags
        count = 0

        matches.update(flags) 

        for n, k in enumerate(matches.keys(), start = 1):
            i, j = k
            a1, a2, R = matches[k]
            flag = 'Match #%03d' % n +': %s -> %s (%s) w/ R^2 = %.3f' % (a1, a2, self.df.identifier[j], R)

            self.df.loc[i, 'adduct_flag'] += ('' if self.df.loc[i, 'adduct_flag'] == '' else ' | ') + flag
            self.df.loc[j, 'adduct_flag'] += ('' if self.df.loc[j, 'adduct_flag'] == '' else ' | ') + 'Match #%03d' % n

            count += 1

        self.log('Flagged %d adduct peaks' % count)



def run_msflo(configuration_file, peak_table_file, file_format, archive_results=False, quiet=False):
    config_handler = parameters.ConfigHandler()
    job = MSFLOProcessor('', peak_table_file, file_format, quiet=quiet)
    config = config_handler.load_file(configuration_file)

    run_msflo_job(job, config, archive_results, quiet=quiet)


def run_msflo_job(job, config, archive_results=True, quiet=False):
    if not quiet:
        logger.info('Starting job %s...' % job.job_id)

    if job.mongo:
        connect(host=job.mongo_host, db=job.mongo_db)

    try:
        data = job.parser.parse_file(job.filename)

        if type(data) == str:
            raise Exception(data)

        df, peak_columns = data
    except Exception as e:
        job.error = traceback.format_exc()
        job.log('Exception when parsing file '+ job.filename)
        job.log(traceback.format_exc())
        job.log('Configuration:')
        job.log(config)
        job.export_log()
        job.save()

        logger.error('Exception when parsing file '+ job.filename)
        logger.error(traceback.format_exc())
        return


    try:
        job.start(df, peak_columns, config)
        job.remove_reference_ions()
        job.remove_duplicates()
        job.isotope_search()
        # job.known_adduct_joiner()
        job.unknown_adduct_joiner()

        job.parser.export_file(job.df, job.filename, 'processed')
        job.export_log()

        if archive_results:
            job.archive_results()

        job.stop()

        if not quiet:
            logger.info('Finished job %s' % job.job_id)
    except Exception as e:
        job.error = traceback.format_exc()
        job.log('Exception when processing '+ job.filename)
        job.log(traceback.format_exc())
        job.log('Configuration:')
        job.log(config)
        job.export_log()
        job.save()

        logger.error('Exception when processing '+ job.filename)
        logger.error(traceback.format_exc())
        return


def merge_task(processor):
    try:
        processor.start()

        processor.merge_changes()
        processor.export_file('merged')
        processor.export_log()
        processor.archive_results()

        processor.stop()

    except Exception as e:
        processor.error = e
        processor.log('Exception when merging', processor.filename)
        processor.log(traceback.format_exc())
