#!/bin/bash

# Name of the MS-FLO docker image
IMAGE_NAME="msflo"
IMAGE_REGISTRY="eros.fiehnlab.ucdavis.edu/$IMAGE_NAME"


# Prepare web app
python3 docker_prepare_app.py || exit 1


# Build docker image
mkdir -p root
cp ../*.py ../*.ini ../requirements.txt root/
cp -r ../msflo root
docker build -t $IMAGE_NAME --rm=true . | tee docker_build.log || exit 1
rm -rf root/


# Tag the docker container
ID=$(tail -1 docker_build.log | awk '{print $3;}')
docker tag $ID $IMAGE_NAME:latest
docker tag $ID $IMAGE_REGISTRY:latest

echo "Tagged $ID as $IMAGE_NAME:latest"
echo "Tagged $ID as $IMAGE_REGISTRY:latest"


# Push if requested
if [ "$1" == "push" ]; then
	echo "pushing $IMAGE to server"
	docker push $IMAGE_REGISTRY
else
	echo "pushing disabled - use 'push' as argument to push to the docker registry"
fi