#!/usr/bin/env python3

import json
import logging
import os
import shutil
import sys
import time
import traceback
import queue
import uuid

from msflo import msflo_processor, parameters, parsers, worker

from mongoengine import connect
from bottle import route, response, request, static_file, run
from pymongo import MongoClient


#
# Configure logging
#
logger = logging.getLogger()
logger.setLevel(logging.INFO)
logging.getLogger('waitress').setLevel(logging.INFO)


#
# Configure database
#

MONGO_HOST = 'venus.fiehnlab.ucdavis.edu'
MONGO_DB = 'msflo'

client = MongoClient(MONGO_HOST)
db = client[MONGO_DB].msflo


#
# Jobs control
#

N_WORKERS = 4

jobs = {}
worker_queue = queue.Queue()

def add_task(task_function, args):
    worker_queue.put((task_function, args))
    


#
# Routing methods
#

@route('/jobs/<job_id>')
def job_status(job_id=None):
    def get_log(job, length=25):
        logs = job['job_log'].splitlines()
        prefix = '[%d previous log lines...]\n' % max(0, job['job_log_length'] - length)
        return prefix + '\n'.join(logs[-length:])

    job = None if job_id is None else db.find_one(job_id)

    if job is None:
        return {'status': 'invalid'}

    elif 'error' in job:
        return {'status': 'error', 'log': get_log(job)}

    elif job['queued']:
        return {'status': 'queued', 'log': 'Waiting for processing slot to open...'}

    elif job['running']:
        return {'status': 'running', 'log': get_log(job)}

    elif not job['queued'] and not job['running'] and 'error' not in job:
        return {'status': 'success', 'log': get_log(job, length=100)}

    else:
        return {'status': 'error', 'error': 'Unknown status'}

@route('/jobs/<job_id>/download')
def job_download(job_id=None):
    job = None if job_id is None else db.find_one(job_id)

    if job is None:
        return 'Job does not exist'

    elif 'error' in job:
        return 'Job experienced a processing error'

    elif job['queued'] or job['running']:
        return 'Job is running'

    elif not job['queued'] and not job['running'] and 'error' not in job:
        return static_file(job['basename'] +'.zip', root='.', download=True)

    else:
        return 'Unknown status'


@route('/', method = 'GET')
def root():
    return static_file('index.html', root = 'static')

@route('<filename:path>', method = 'GET')
def static_content(filename):
    return static_file(filename, root = 'static')


@route('/', method = 'POST')
def process():
    logger.info('Accepting POST request...')

    # Get POST data
    try:
        if 'parameters' in request.forms:
            params = request.forms.parameters
        else:
            params = request.files.ini_file

        file_format = request.forms.file_format
        peak_table = request.files.peak_table

        data = peak_table.file.read()
    except Exception:
        logger.info(traceback.format_exc())
        return {'status': 'error', 'error': 'Error retriving POST data: '+ traceback.format_exc()}


    # Validate parameters
    try:
        config_handler = parameters.ConfigHandler()

        if type(params) is str:
            params_json = json.loads(params)
            config = config_handler.load_data(params_json)
        else:
            params_data = params.file.read().decode('utf-8').splitlines()
            config = config_handler.load_text(params_data)
    except Exception:
        logger.error(traceback.format_exc())
        return {'status': 'error', 'error': 'Error parsing parameters: '+ traceback.format_exc()}
    

    # Generate UUID and directory structures
    try:
        job_id = str(uuid.uuid1())
        dirname = 'data/%s/' % job_id
        peak_table_filename = dirname + peak_table.filename
        peak_table_basename = '.'.join(peak_table_filename.split('.')[:-1])

        logger.info('\tInitializing job %s' % job_id)

        # Create directory
        os.makedirs(dirname)

        # Write data file
        with open(peak_table_filename, 'wb') as f:
            f.write(data)

        # Write parameters file
        config_handler.export_config(peak_table_basename +'_parameters.ini', config)
    except Exception:
        logger.error(traceback.format_exc())
        return {'status': 'error', 'error': 'Error creating directory structure/storing data file: '+ traceback.format_exc()} 


    # Validate peak table file
    try:
        handler = parsers.get_handler(file_format)

        if not handler.validate_file(peak_table_filename):
            logger.info('\tInvalid file format for job %s' % job_id)
            shutil.rmtree(dirname)
            return {'status': 'error', 'error': 'Invalid %s peak table file!' % file_format}
    except Exception:
        logger.error(traceback.format_exc())
        return {'status': 'error', 'error': 'Error validating peak table file: '+ traceback.format_exc()} 
  

    # Create processing job
    try:
        # Add processing job to the queue and jobs list
        job = msflo_processor.MSFLOProcessor(job_id, peak_table_filename, file_format, 
            mongo=True, mongo_host=MONGO_HOST, mongo_db=MONGO_DB)
        job.log('Creating job with ID: %s' % job_id)
        jobs[job_id] = job
        
        add_task(msflo_processor.run_msflo_job, (job, config))
        logger.info('\tAdded job %s to queue' % job_id)
    except Exception:
        logger.error(traceback.format_exc())
        return {'status': 'error', 'error': 'Error initializing processing job: '+ traceback.format_exc()} 


    # Return queued response code
    return {'status': 'queued', 'id': job_id}
 

if __name__ == '__main__':
    # Get all existing jobs
    if not os.path.exists('data'):
        os.makedirs('data')

    # Set worker thread as daemon thread and start
    for i in range(N_WORKERS):
        w = worker.Worker(worker_queue)
        w.daemon = True
        w.start()

        print('Staring thread #%d:' % i)

    # Set up MongoDB connection
    connect(host=MONGO_HOST, db=MONGO_DB)

    # Start http server
    run(server = 'waitress', host = '0.0.0.0', port = 8080)
